<?php
    session_start();
    require_once('includes/config_autologin.php');
    setcookie ($cookie_name, '',time()-60*60*24*365);
    $_SESSION['auth']='0';
    $_SESSION['email'] = ''; 
    $_SESSION['name'] = ''; 
    header('Location: '.$url.'/login.php?logout=1');
?>