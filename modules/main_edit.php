<div class="page-header">
        <h1>עריכת פרוייקט</h1>
    </div>
<ul class="breadcrumb">
<?php
$url2="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$wizard_menu = $_GET['tab'];
switch ($wizard_menu)
{
    case 1:
            $tab1 = ' class="active"';
            $content_file = 'main_edit_tab1.php';
            break;
    case 2:
            $tab2 = ' class="active"';
            $content_file = 'main_edit_tab2.php';
            break;
    case 3:
            $tab3 = ' class="active"';
            $content_file = 'main_edit_tab3.php';
            break;
    case 4:
            $tab4 = ' class="active"';
            $content_file = 'main_edit_tab4.php';
            break;
    case 5:
            $tab5 = ' class="active"';
            $content_file = 'main_edit_tab5.php';
            break;
    case 6:
            $tab6 = ' class="active"';
            $content_file = 'main_edit_tab6.php';
            break;
    default:
            $tab1 = ' class="active"';
            $content_file = 'main_edit_tab1.php';
            break;
    
}
?>
  <li<?=$tab1?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=1">פרטי הפרוייקט</a> <span class="divider">/</span></li>
  <li<?=$tab2?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=2">דירות</a> <span class="divider">/</span></li>
  <li<?=$tab6?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=6">סוגי דירות</a> <span class="divider">/</span></li>
  <li<?=$tab3?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=3">סכימת דירות</a> <span class="divider">/</span></li>
  <li<?=$tab4?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=4">לוגו חברה</a> <span class="divider">/</span></li>
  <li<?=$tab5?>><a href="main_edit.php?id=<?=$_GET['id']?>&tab=5">משתמשים</a></li>
</ul>
<?php 
include($content_file);
?>
<BR><BR>
<div style="float:right;"><button class="btn"><<</button></div>
<div style="float:left;"><button class="btn">>></button></div>