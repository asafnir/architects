<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/" style="padding:3px 0 0 20px; margin:0;"><img src="img/logo_header.png"></a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li<?=$ma?>><a href="../main.php">פרוייקטים</a></li>
                    <li<?=$it?>><a href="../items.php">פריטים</a></li>
                    <?php
                        if ($_SESSION['role'] > 999){
                    ?>
                    <li><a href="#">|</a></li>
                    <li class="<?=$ad?>dropdown">
                <a class="dropdown-toggle" id="drop5" role="button" data-toggle="dropdown" href="#">מנהל <b class="caret"></b></a>
                <ul id="menu3" class="dropdown-menu" role="menu" aria-labelledby="drop5">
                  <li><a tabindex="-1" href="/admin_users.php">משתמשים</a></li>
                  <li><a tabindex="-1" href="/admin_roles.php">תפקידים</a></li>
                  <li><a tabindex="-1" href="/admin_reports.php">דוחות</a></li>
                  <li class="divider"></li>
                  <li><a tabindex="-1" href="/admin_settings.php">הגדרות</a></li>
                </ul>
              </li>
              <?php
                        }
              ?>
                </ul>
                <ul class="nav" style="float:left;padding-right:0;margin-right:0;">
                    <li<?=$ac?>><a href="/account.php"><?php echo $_SESSION['name']?></a></li>
                    <li><a href="#">|</a></li>
                    <li><a href="/logout.php">התנתק</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>