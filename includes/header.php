<?php
include('functions.php');
$active_menu=$_SERVER["REQUEST_URI"][1].$_SERVER["REQUEST_URI"][2];
$xx = ' class="active"';
switch ($active_menu)
{
    case ma:
            $ma = $xx;
            break;
    case se:
            $se = $xx;
            break;
    case ad:
            $ad = 'active ';
            break;
    case it:
            $it = $xx;
            break;
    case ac:
            $ac = $xx;
            break;
    default:
            $ma = $xx;
            break;
    
}
?>
<!DOCTYPE HTML>
<html lang="he">
<head>
<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta charset="utf-8">
<title>aDoc</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<!-- Bootstrap CSS Toolkit styles -->
<!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
<link rel="stylesheet" href="bootstrap/css/bootstrap-rtl.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="css/style.css">
<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
<link rel="stylesheet" href="bootstrap/css/bootstrap-responsive-rtl.min.css">
<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
<!-- Bootstrap Image Gallery styles -->
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="css/jquery.fileupload-ui.css">
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="bootstrap/js/jquery-1.8.1.min.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-modal.js"></script>
<script src="js/bootbox.js"></script>
</head>
<body>
<div class="container">
